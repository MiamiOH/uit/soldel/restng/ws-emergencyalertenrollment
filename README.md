# ws-emergencyAlertEnrollment

A RESTNG web service that enrolls a phone number into the Omnilert emergency alert system.

## Resources
- [EmergencyAlertEnrollment on Swagger](https://wsdev.apps.miamioh.edu/api/swagger-ui/#/EmergencyAlertEnrollment)
- [ws-emergencyAlertEnrollment TD Asset Information](https://miamioh.teamdynamix.com/TDNext/Apps/741/Assets/AssetDet?AssetID=838736)

