<?php

return [
    'resources' => [
        'emergencyAlertEnrollment' => [
            \MiamiOH\EmergencyAlertEnrollmentService\Resources\EmergencyAlertEnrollmentResourceProvider::class,
        ]
    ]
];