<?php
namespace MiamiOH\EmergencyAlertEnrollmentService\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use MiamiOH\RESTng\App;
use Illuminate\Support\Facades\Log;
use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Exception\DataSourceNotFound;

class EmergencyAlertEnrollmentSubscriber extends \MiamiOH\RESTng\Service
{
    private $dataSource = 'OMNILERT_API';

    /** @var  \MiamiOH\RESTng\Connector\DataSourceFactory $dsFactory */
    private $dsFactory;
    private $baseUrl;
    private $accountKey;
    private $username;
    private $password;


    public function setDataSourceFactory($dsFactory)
    {
        /** @var  \MiamiOH\RESTng\Connector\DataSourceFactory $dsFactory */
        $this->dsFactory = $dsFactory;
    }

    public function getSubscriberDetail(): \MiamiOH\RESTng\Util\Response
    {
        $this->getDataSource();

        $request = $this->getRequest();
        $response = $this->getResponse();
        $email = $request->getResourceParam('email');

        $resp=$this->getSubscriberInfo($email);
        if($resp->getStatusCode() == '200') {
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload(json_decode($resp->getBody(),1));
        }else{
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            $response->setPayload(json_decode($resp->getBody(), 1));
        }

        return $response;
    }

    public function addSubscriberSMS(): \MiamiOH\RESTng\Util\Response
    {
        $this->getDataSource();

        $request = $this->getRequest();
        $response = $this->getResponse();
        $data = $request->getData();
        $email = $data['email'];
        $phone = $data['phone'];

        $environment = $this->app->getCurrentStage();
        if($environment == 'production' || $environment == 'testing') {
            $apiResponse = $this->apiSMSAdd($email, $phone);
            if ($apiResponse->getStatusCode() == '201') {
                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
                $response->setPayload(json_decode($apiResponse->getBody(), 1));
            } else {
                $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
                $response->setPayload(json_decode($apiResponse->getBody(), 1));

            }
        }else{
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload(['message' => 'Functionality disabled in '. $environment], 1);
        }

        return $response;
    }

    /**
     * @codeCoverageIgnore
     */
    public function apiSMSAdd($email, $phone): \GuzzleHttp\Psr7\Response{
        $url = $this->baseUrl . 'subscribers/' . $email.'/sms';
        $headers = [
            'Authorization' => 'Basic ' . base64_encode($this->username . ':' . $this->password),
            'Content-Type' => 'application/x-www-form-urlencoded',
            'omnilert_account_key' => $this->accountKey,
        ];

        $OARequest = new Request('POST', $url, $headers,'phone='.urlencode($phone).'&carrier_id=0');
        $client = new Client();
         try {
             return $client->send($OARequest);
         }catch(\Exception $e){
             $parsedString = explode(':' , $e->getMessage());
             return new \GuzzleHttp\Psr7\Response(404,[], json_encode(['message' => trim($parsedString[count($parsedString)-1])]));
         }
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscriberInfo($email): \GuzzleHttp\Psr7\Response{
        $url=$this->baseUrl.'subscribers/'.$email;
        $headers=[
            'Authorization'=>'Basic '. base64_encode($this->username.':'.$this->password),
            'Content-Type'=>'application/json; charset=utf-8',
            'omnilert_account_key'=>$this->accountKey
        ];
        $OARequest=new Request('GET',$url,$headers);
        $client = new Client();
        try {
            $client->send($OARequest);
            return $client->send($OARequest);
        }catch (\Exception $e){
            //$parsedString = $e->getMessage();
            return new \GuzzleHttp\Psr7\Response(404,[], json_encode(['message' => "Unable to get subscriber info"]));
        }
    }

    /**
     * @codeCoverageIgnore
     */
    public function getDataSource()
    {
        $dataSource = $this->dsFactory->getDataSource($this->dataSource);
        $this->baseUrl = $dataSource->getHost();
        $this->accountKey = $dataSource->getDatabase();
        $this->username = $dataSource->getUser();
        $this->password = $dataSource->getPassword();
        return $dataSource;
    }

    public function setApp(App $app): void
    {
        $this->app = $app;
    }

    public function setDatabase($database)
    {
    }

}
