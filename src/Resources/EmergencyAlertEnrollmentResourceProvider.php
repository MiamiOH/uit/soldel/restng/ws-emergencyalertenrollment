<?php

namespace MiamiOH\EmergencyAlertEnrollmentService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EmergencyAlertEnrollmentResourceProvider extends ResourceProvider
{
    private $tag="EmergencyAlertEnrollment";

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'EmergencyAlertEnrollment.Subscriber',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'number',
                ),
                'username' => array(
                    'type' => 'string',
                ),
                'devices' => array(
                    '$ref' => '#/definitions/EmergencyAlertEnrollment.Subscriber.Devices',
                )
            ),
        ));


        $this->addDefinition(array(
            'name' => 'EmergencyAlertEnrollment.Subscriber.Devices',
            'type' => 'object',
            'properties' => array(
                'email' => array(
                    '$ref' => '#/definitions/EmergencyAlertEnrollment.Subscriber.Devices.email'
                ),
                'sms' => array(
                    '$ref' => '#/definitions/EmergencyAlertEnrollment.Subscriber.Devices.SMS'
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'EmergencyAlertEnrollment.Subscriber.Devices.email',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'number',
                ),
                'email' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'EmergencyAlertEnrollment.Subscriber.Devices.SMS',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'number',
                ),
                'user_id' => array(
                    'type' => 'string',
                ),
                'phone' => array(
                    'type' => 'string',
                ),
            ),
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'EmergencyAlertEnrollmentSubscriber',
            'class' => 'MiamiOH\EmergencyAlertEnrollmentService\Services\EmergencyAlertEnrollmentSubscriber',
            'description' => 'Provide the emergency alert subscriber given an email.',
            'set' => array(
                'dataSourceFactory' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),
                'app' => ['type' => 'service', 'name' => 'APIApp'],
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                )
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read',
                'name' => 'emergencyAlertEnrollment.subscriber.read',
                'description' => "Obtain subscriber detail",
                'pattern' => '/emergencyAlertEnrollment/subscriber/v1/:email',
                'service' => 'EmergencyAlertEnrollmentSubscriber',
                'method' => 'getSubscriberDetail',
                'isPageable' => false,
                'tags' => array('EmergencyAlertEnrollment'),
                'returnType' => 'model',
                'params' => array(
                    'email' => array(
                        'description' => 'A Miami email address',
                    ),
                ),
                'middleware' => [
                    'authenticate' => ['type' => 'token'],
                    'authorize' => [
                        'application' => 'WebServices',
                        'module' => $this->tag,
                        'key' => ['view', 'all']
                    ],
                ],
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Status code indicating status of subscriber retrieval',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/EmergencyAlertEnrollment.Subscriber',
                        )
                    ),
                )
            )
        );

        $this->addDefinition([
            'name' => 'phone.post.model',
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'phone' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ]
            ]
        ]);

        $this->addResource(array(
                'action' => 'create',
                'name' => 'emergencyAlertEnrollment.subscriber.update',
                'description' => "Obtain subscriber detail",
                'pattern' => '/emergencyAlertEnrollment/subscriber/v1/phone',
                'service' => 'EmergencyAlertEnrollmentSubscriber',
                'method' => 'addSubscriberSMS',
                'isPageable' => false,
                'tags' => array('EmergencyAlertEnrollment'),
                'returnType' => 'model',
                'body' => [
                    'required' => true,
                    'description' => 'Person Data',
                    'schema' => [
                        '$ref' => '#/definitions/' . 'phone.post.model'
                    ]
                ],

                'middleware' => [
                    'authenticate' => ['type' => 'token'],
                    'authorize' => [
                        'application' => 'WebServices',
                        'module' => $this->tag,
                        'key' => ['create', 'all']
                    ],
                ],
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Status code indicating status of subscriber retrieval',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/EmergencyAlertEnrollment.Subscriber',
                        )
                    ),
                )
            )
        );
    }

    public function registerOrmConnections(): void
    {

    }
}