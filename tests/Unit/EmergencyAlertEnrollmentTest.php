<?php

namespace MiamiOH\EmergencyAlertEnrollmentService\Tests\Unit;

use GuzzleHttp\Psr7\Response;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\DataSource;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\EmergencyAlertEnrollmentService\Services\EmergencyAlertEnrollmentSubscriber;

class EmergencyAlertEnrollmentTest extends TestCase
{
    /*************************/
    /**********Set Up*********/
    /*************************/
    private $alertService;

    private $mockModel = [];
    private $updateModel = [];
    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];
    /** @var App|MockObject */
    private $restngApp;


    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp(): void
    {
        $this->mockModel = [];
        $this->updateModel = [];
        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];

        //set up the mock api:

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(array('getResourceParam', 'getData'))
            ->getMock();

        $this->alertService = $this->getMockBuilder(EmergencyAlertEnrollmentSubscriber::class)
            ->setMethods(array('getSubscriberInfo', 'getDataSource', 'apiSMSAdd'))
            ->getMock();

        $this->alertService->method('getDataSource')
            ->willReturn(null);

        $this->restngApp = $this->getMockBuilder(App::class)
            ->setMethods(array('getCurrentStage'))
            ->getMock();

        $this->restngApp->method('getCurrentStage')->willReturn('testing');
        $this->alertService->setApp($this->restngApp);

        $this->request->method('getData')
            ->will($this->returnCallback(array($this, 'getDataMock')));
    }
//
//    /*************************/
//    /**********Tests**********/
//    /*************************/
//
    public function testGetSubscriberDoesNotExist()
    {
        $this->mockModel = [
            [
                'email' => 'testmctest@miamioh.edu',
            ]
        ];
        $this->alertService->method('getSubscriberInfo')
            ->willReturn(new Response(404, [], '{ "message": "Subscriber not found."}'));

        $this->alertService->setRequest($this->request);

        $response = $this->alertService->getSubscriberDetail();
        $payload = $response->getPayload();


        $this->assertEquals(App::API_NOTFOUND, $response->getStatus());
    }

    public function testGetSubscriber()
    {
        $this->mockModel = [
            [
                'email' => 'testmctest@miamioh.edu',
            ]
        ];
        $webAPIResponse = '{"id": "12345",
    "username": "testmctest@miamioh.edu",
    "validated": "1",
    "access_level": "1",
    "account_expiration_date": "0000-00-00",
    "firstname": "Test",
    "lastname": "McTest",
    "deleted": "0",
    "optout_date": "0000-00-00 00:00:00",
    "preferred_languages": "",
    "personal_access_code": "abcd",
    "tags": null,
    "groups": "Oxford Campus",
    "devices": {
      "email": [
        {
          "id": "1234",
          "user_id": "41335255",
          "email": "testmctest@miamioh.edu",
          "validation_status": "1",
          "active_status": "1"
        }
      ],
      "sms": [
        {
          "id": "123456",
          "user_id": "12345",
          "phone": "5135231234",
          "carrier_id": "999",
          "validation_status": "1",
          "active_status": "1",
          "carrier_name": "Other"
        }
      ]
    }}';
        $this->alertService->method('getSubscriberInfo')
            ->willReturn(new Response(200, [], $webAPIResponse));

        $this->alertService->setRequest($this->request);

        $response = $this->alertService->getSubscriberDetail();
        $payload = $response->getPayload();
        $this->assertEquals(App::API_OK, $response->getStatus());
        $this->assertEquals(json_decode($webAPIResponse, 1), $payload);
    }

    public function testAddSMSPhoneExists()
    {
        $this->mockModel = [

                'phone' => '5555555555',
                'email' => 'tesetmctest@miamioh.edu',

        ];

        $this->alertService->method('apiSMSAdd')
            ->willReturn(new Response(400, [], '{ "message": "SMS number already in use by this user."}'));

        $this->alertService->setRequest($this->request);

        $response = $this->alertService->addSubscriberSMS();
        $payload = $response->getPayload();


        $this->assertEquals(App::API_BADREQUEST, $response->getStatus());
    }

    public function testAddSMSUserDoesntExist()
    {
        $this->mockModel = [

                'phone' => '5555555555',
                'email' => 'tesetmctest@miamioh.edu',

        ];

        $this->alertService->method('apiSMSAdd')
            ->willReturn(new Response(400, [], '{ "message": "Record not found."}'));

        $this->alertService->setRequest($this->request);

        $response = $this->alertService->addSubscriberSMS();
        $payload = $response->getPayload();


        $this->assertEquals(App::API_BADREQUEST, $response->getStatus());
    }

    public function testAddSMSAlreadyAdded()
    {
        $this->mockModel = [

                'phone' => '5555555555',
                'email' => 'tesetmctest@miamioh.edu',

        ];

        $this->alertService->method('apiSMSAdd')
            ->willReturn(new Response(400, [], '{ "message": "SMS number already in use by this user."}'));

        $this->alertService->setRequest($this->request);

        $response = $this->alertService->addSubscriberSMS();
        $payload = $response->getPayload();


        $this->assertEquals(App::API_BADREQUEST, $response->getStatus());
    }

    public function testAddSMSUser()
    {
        $this->mockModel = [

                'phone' => '5555555555',
                'email' => 'tesetmctest@miamioh.edu',

        ];

        $this->alertService->method('apiSMSAdd')
            ->willReturn(new Response(201, [], '{"id": "12345",
    "username": "testmctest@miamioh.edu",
    "validated": "1",
    "access_level": "1",
    "account_expiration_date": "0000-00-00",
    "firstname": "Test",
    "lastname": "McTest",
    "deleted": "0",
    "optout_date": "0000-00-00 00:00:00",
    "preferred_languages": "",
    "personal_access_code": "abcd",
    "tags": null,
    "groups": "Oxford Campus",
    "devices": {
      "email": [
        {
          "id": "1234",
          "user_id": "41335255",
          "email": "testmctest@miamioh.edu",
          "validation_status": "1",
          "active_status": "1"
        }
      ],
      "sms": [
        {
          "id": "123456",
          "user_id": "12345",
          "phone": "5555555555",
          "carrier_id": "999",
          "validation_status": "1",
          "active_status": "1",
          "carrier_name": "Other"
        }
      ]
    }}'));

        $this->alertService->setRequest($this->request);

        $response = $this->alertService->addSubscriberSMS();
        $payload = $response->getPayload();


        $this->assertEquals(App::API_OK, $response->getStatus());
    }

    public function getDataMock()
    {
        return $this->mockModel;
    }
}
